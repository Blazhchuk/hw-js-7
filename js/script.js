"use strict"

/*

1. Рядки можно створити за допомогою лапок '', "", ``.

2. В зворотніх лапках можна додавати змінні та додатковій текст за допомогою ${}. 

3. Перевірити чи рядкі півні між собою можна за допомоги таких операторів як дорівнює == або суворе дорівнює ===.

4. Date.now() повертає кількість мілісекунд що пройшло з 1 січня 1970р

5. Date.now() new Date() - це два сопосби отримати поточний час. new Date() створює новий обєкт який показує поточні дату та час

*/


// ex 1

function isPalindrome(str) {
    str = str.toLowerCase().replace(/[?!.' ]/g, '');
    let reversedStr = str.split('').reverse().join('');
    return str === reversedStr;
}

// console.log(isPalindrome('А результатів? Вітать лузера!'));
// console.log(isPalindrome('Hello World'));


// ex 2

function checkLength(str, maxLength) {
    return str.length <= maxLength;
}

// console.log(checkLength('Hello', 5));
// console.log(checkLength('Hello', 3));
// console.log(checkLength('Hello, world!', 20));


// ex 3

function fullYear() {
    let birth = prompt('Enter your date birth in form DD.MM.YYYY');
    let checkData = birth.split('.');

    if (checkData.length !== 3 || checkData[0].length !== 2 || checkData[1].length !== 2 || checkData[2].length !== 4) {
        console.log('Enter correct date in format DD.MM.YYYY');
        return;
    }

    birth = new Date(checkData[2], checkData[1] - 1, checkData[0]);

    if (isNaN(birth.getTime())) {
        console.log('Enter correct date in format DD.MM.YYYY');
        return;
    }

    let today = new Date();
    let age = today.getFullYear() - birth.getFullYear();
    let month = today.getMonth() - birth.getMonth();

    if (month < 0 || (month === 0 && today.getDate() < birth.getDate())) {
        age--;
    }

    return age;

}

console.log(fullYear());

